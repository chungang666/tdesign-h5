import { createApp } from "vue";
import { store } from "./store";
// normalize.css
import "normalize.css/normalize.css";
// 全局样式
import "./styles/index.less";
// tailwindcss
import "./styles/tailwind.css";
// 引入组件库的少量全局样式变量
import "tdesign-mobile-vue/es/style/index.css";
// svg icon
import "virtual:svg-icons-register";

import App from "./App.vue";
import router from "./router";

const app = createApp(App);
app.use(store);
app.use(router);

app.mount("#app");
