import { http } from "@/utils/http";
import qs from 'qs'
export function getToken(params: {
  grant_type: string;
  appid: string;
  secret: string;
}) {
  return http.request({
    url: "/cgi-bin/token",
    method: "get",
    params
  });
}

/**
 * 获取不限制的小程序码 https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/qrcode-link/qr-code/getUnlimitedQRCode.html#%E6%8E%A5%E5%8F%A3%E8%AF%B4%E6%98%8E
 * @param params 
 * @returns 
 */
export function getwxacodeunlimit(params: {
  access_token: string;
  scene: string;
  page: string;
}) {
  return http.request<{
    buffer: Buffer;
    errcode: number;
    errmsg: string;
  }>({
    url: "/wxa/getwxacodeunlimit?access_token=" + params.access_token,
    method: "post",
    headers: {
      "Content-Type": 'application/json'
    },
    data: {
      scene: params.scene,
      page: params.page,
      env_version: 'develop',
      check_path: false
    }
  });
}


/**
 * 获取加密scheme码 https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/qrcode-link/url-scheme/generateScheme.html
 * @param params 
 * @returns 
 */
export function generateUrlLink(params: {
  access_token: string;
  query?: string;
  page: string;
}) {
  return http.request<{
    buffer: Buffer;
    errcode: number;
    errmsg: string;
  }>({
    url: "/wxa/generate_urllink?access_token=" + params.access_token,
    method: "post",
    headers: {
      "Content-Type": 'application/json'
    },
    data: {
      path: params.page,
      env_version: 'develop'
    }
  });
}
